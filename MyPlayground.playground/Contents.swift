import UIKit
// Arrays
var array1 = [Int] ()
print("Empty is \(array1.isEmpty)")
array1.append(5)
array1 += [7,9]
print("index 0 : \(array1[1])")
array1[0] = 4
print(array1)
array1.insert(5, at: 3)
print(array1)
array1.remove(at: 3)
print(array1)
array1[0...2] = [1,2,3,4]
print(" \n length: \(array1.count)")
var array2 = Array(repeating: 5, count: 5)
var array3 = array1 + array2
for item in array3 {
    print(item)
}
for (key,values) in array3.enumerated() {
    print(" \(key) \(values)")
}
var array4 = Array(1...6)
print("Array : \(array4[0...5])")
array2[1..<4] = [9,8]
print("Array : \(array4)")
array4[1..<1] = [10,11]
print("Array : \(array4)")
print("11th index : \(array4.firstIndex(of: 11)!)")
print("Contains : \(array4.contains(11))")
print("Min : \(array4.min()!)")
print("Max : \(array4.max()!)")
array4.sort()
array4.sort{$0 > $1}
var array5 = [[1,2,3],[4,5,6],[7,8,9]]
print("Array : \(array5[1][0])")
var array6 = Array(array5.joined())
print("Array : \(array6.split{$0.isMultiple(of: 2)}))")
var array7 = array6.filter{$0.isMultiple(of: 2)}
print(array7)
var array8 = array6.filter{$0 > 5}
print("Less than : \(array6.allSatisfy({$0 < 12}))")
var array9 = array6.map{$0 * 2}
print("Sum : \(array9.reduce(0){$0 + $1})")

//Dictionaries

var dictionary = [Int : String]()
print("Empty dictionary : \(dictionary.isEmpty)")

var dict : [String : String] = ["1" : "Nagu","2" : "Navy","3": "nagendran"]
print("my dictionary name count is : \(dict.count)")

var names : [String : String] = ["178918" : "BCA"]
for (index,values) in names {
    print("Rollno is \(index) and Dept are \(values) ")
}
//string
var str = ""
print(" Its Empty String \(str.isEmpty)")
str = "my status is single"
print("String is Empty \(str.isEmpty)")
var str1 = "its is always against form coumunites " + (str)
print(str1)
print("Count : \(str1.count)")
print("Start Index : \(str1[str1.startIndex])")
let index1 = str1.index(str1.startIndex,offsetBy: 5)
print("5th is \(str1[index1])")
print("s in String : \(str1.contains("s"))")
print("Vowels in : \(str1.contains{"aeiou".contains($0)})")
print("Only vowels : \("nagu".filter{"aeiou".contains($0)})")
print("1 st 4 line is : \(String(str1.prefix(4)))")

let arr = str1.split{$0 == " "}
for i in arr {
    print("\(i)")
}
str1.remove(at: str1.startIndex)
str1.insert("A", at: str1.startIndex)
let index6 = str1.index(str1.startIndex, offsetBy: 15)
str1.insert(contentsOf: " is great", at: index6)
print("\(str1)")

// Structures
struct Rectangle {
    var height = 0.0
    var length = 0.0
    func area() -> Double {
        let area = height * length
        return area
    }
}
let myRectangle = Rectangle(height: 10.0, length: 10.0)
print("Area : \(myRectangle.height) * \(myRectangle.length) = \(myRectangle.area())")

// struct program

struct Person {
    var pen : String?
    var pencil : String?
    func note() {
        print("its my \(pen!) pen and \(pencil!) pencil")
    }
}
let status = Person(pen: "hero", pencil: "apsara")
print("\(status.note())")

// operators

var a: Int = 10
var b: Int = 20
if (a==b) {
    print("it is correct")
}else {
    print("it is wrong")
}
// closures
var named = ["nagu","navy","raju","raghu","vel"]
func back(_ s1:String , _ s2:String) -> Bool {
    return s1<s2
}

var story = named.sorted() {($0 > $1)}
print(story)

var myfunc: ((Int) -> Bool) = { number in // variables & properties
   if number > 3 {
        return true
    }
    return false
}
let its = myfunc(4)
print(its)

func open(number: Int) -> Bool{ //function
    if number > 2 {
        return true
}
return false
}
let result = open(number :2)
print(result)

var a0 : Int? = 10
var b0 : Int? = 90
if var value = a0 {
    if var value1 = b0 {
        let result1 = value + value1
        print(result1)
    }
}
var price: Double?
print(price)
price = 2.00
print(price!)

// init()
class muliple {
    var a1 : Int = 10
    var b1 : Int = 10
    var c1 : Int?
init(_ a1 :Int,_ b1:Int){
    self.a1 = a1
    self.b1 = b1
}
    func mul(){
        c1 = a1 * b1
        print("multiplication : \(c1!)")
    }
}
var m = muliple(10,11)
m.mul()

// Terminator
for inn in 1...10{
    print(inn * 10, terminator: " ")
}
print(" ")
for u in 1...10{
    for p in 1...10{
        print(u * p, terminator: " ")
    }
    print(" ")
}

let students1 = ["nagu","rajesh","hari"]
let check = "viki"
if students1.contains(check){
    print("\(check) is signed up")
}
else{
    print("\(check) has no signed")
}


// Alert message
import UIKit


class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        }
    override func viewDidAppear(_ animated: Bool) {
        createAlert(title: "DO YOU LIKE SAUGE?", message: "DO YOU?")
    }
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // creating Button
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { (action) in
                                                                                                  alert.dismiss(animated: true,completion: nil)
        }))
        print("Yes")
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { (action) in
                                                                                                  alert.dismiss(animated: true,completion: nil)
        }))
        print("NO")
        
        self.present(alert, animated: true,completion: nil)
    }
}

// PageviewConroller
import UIKit

class ViewController: UIViewController {
    var pageViewController: UIPageViewController!
    lazy var viewControllers: [UIViewController] = {
        
        let greenVC = UIViewController()
        greenVC.view.backgroundColor = UIColor.green
        
        
        let yellowVC = UIViewController()
        yellowVC.view.backgroundColor = UIColor.yellow
        
        
        let blueVC = UIViewController()
        blueVC.view.backgroundColor = UIColor.blue
        
        return [greenVC, yellowVC, blueVC]
        
    }()
    
     override func viewDidLoad() {
        super.viewDidLoad()
        pageViewController = UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
        pageViewController.setViewControllers([viewControllers[0]], direction: .forward, animated: true, completion: nil)
        
        pageViewController.dataSource = self
        self.addChild(pageViewController)
        self.view.addSubview(pageViewController.view)
        pageViewController.view.frame = self.view.frame
        pageViewController.didMove(toParent: self)
        
        
    }
}
extension ViewController: UIPageViewControllerDataSource{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let index = viewControllers.firstIndex(of: viewController) else {
            return nil
        }
        let reducedIndex = index - 1
        guard reducedIndex >= 0 else {
            return nil
        }
        guard viewControllers.count > reducedIndex else {
            return nil
        }
        return viewControllers[reducedIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let index = viewControllers.firstIndex(of: viewController) else {
            return nil
        }
        let increaseIndex = index + 1
        guard increaseIndex >= 0 else {
            return nil
        }
        guard viewControllers.count > increaseIndex else {
            return nil
        }
        return viewControllers[increaseIndex]
        
    }
}












 














































